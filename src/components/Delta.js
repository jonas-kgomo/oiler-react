function Delta() {
  return (
 <div class="mx-auto  max-w-6xl border-gray-400 border  w-full bg-white rounded-xl shadow-md overflow-hidden ">
        <div class="md:flex ">
      
       <div class="p-8 md:w-1/2 sm:w-full">
         <p class="text-2xl font-semibold">DELTA PLUS GROUP</p>
         <p class="text-gray-500">DLTA.PA</p>
         <div class="flex uppercase">
           <button class="bg-indigo-200 hover:bg-indigo-800 p-2 m-2 rounded-sm">
             AUSTRALIA </button>
            <button class="bg-indigo-200 hover:bg-indigo-800 p-2 m-2 rounded-sm">
            MATERIALS</button>
        </div>
        
        <div class="flex text-gray-700 justify-between w-1/2"> <p class="text-gray-400">Current Price </p> <b>13.98M</b></div>

        <div class="flex text-gray-700 justify-between w-1/2"> <p class="text-gray-400">Target Price </p> <b>13.98M</b></div>
           
        <div class="flex text-gray-900 justify-between w-1/2"> <p class="text-gray-400">Market Cap </p> <b>10.75B</b></div>


       </div>
  
      <div class="p-8 md:w-1/2 sm:w-full grid justify-items-stretch">
         <div class="md:justify-self-end sm:justify-self-center ">

           <div class="tracking-wide mb-4 rounded-xl p-3 uppercase flex text-sm bg-green-300 text-green-700 font-semibold">
            <img src="./up.svg" class="w-4 mx-2" alt="tick"/>
          LONG POSITION
     
        </div>

        <div class="flex-wrap text-gray-700 "> 
       
        <p class="text-gray-400 flex m-2">  <p class="h-3 w-3 m-2 font-semibold bg-blue-700 rounded-full"></p>
         <p>Expected Returns </p></p> <b class="text-right">20.84%</b></div>

        <div class="flex-wrap text-gray-700 m-2"> 
        <p class="text-gray-400 flex">  <p class="h-3 w-3 m-2 font-semibold bg-blue-700 rounded-full"></p>
          Inception Price </p> <b>$1.20</b></div>
           
        <div class="flex-wrap text-gray-900 m-2"> 
        <p class="text-gray-400 flex">  <p class="h-3 w-3 m-2 font-semibold bg-blue-700 rounded-full"></p>
          Target Price</p> <b>$1.20</b></div>

      
         </div>
          
        
        </div>
  </div>
</div>
     );
}

export default Delta;
