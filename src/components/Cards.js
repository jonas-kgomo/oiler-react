
function Cards() {
  return (
  <div class="md:flex sm:flex-wrap mt-32">
     <div class="max-w-md mx-auto md:w-2/3 sm:w-full md:h-60 sm:h-full bg-indigo-100 rounded-2xl shadow-md overflow-hidden md:max-w-2xl">
          <div class="md:flex p-8">

            <div class="md:flex-shrink-0">
               <h class="text-3xl font-medium">  Submit your stock idea <br/>
               in three simple steps</h>
           
                <p class="flex my-2"> <img src="./tick.svg" class="w-6 mx-2" alt="tick"/> Select your stock</p>
                <p class="flex my-2"> <img src="./tick.svg" class="w-6 mx-2" alt="tick"/> Write your analysis</p>
                <p class="flex my-2"> <img src="./tick.svg" class="w-6 mx-2" alt="tick"/> Allocate virtual dollars to your idea</p>
                 

            </div>

            <div class="p-8 mt-20">
                <div class="tracking-wide rounded-xl p-3 flex text-sm bg-white text-blue-500 font-semibold">Submit a stock idea 
                <img src="./right.svg" class="w-4 mx-2 " alt="tick"/>
                </div>
            </div>
        </div>
    </div>

     <div class="max-w-sm md:w-1/3 sm:w-full p-8 md:h-60 sm:h-full  mx-auto border-green-600 bg-blue-50 rounded-2xl shadow-md overflow-hidden md:max-w-2xl">
          <div class="md:flex-wrap">
            <div class="text-3xl font-medium text-green-900"> Need some <br/>Inspiration of <br/> Stock ideas?</div>
            
            <div class="p-4">
                <div class="tracking-wide rounded-xl p-3 flex text-sm bg-white text-blue-500 font-semibold">Watch our Guidelines 
                <img src="./right.svg" class="w-4 mx-2 " alt="tick"/>
                </div>
            </div>
            </div>
    </div>
  </div>
     );
}

export default Cards;
