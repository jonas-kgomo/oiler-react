import './App.css';
import Card from './components/Cards';
import Delta from './components/Delta';

function App() {
  return (
    // max-width-7xl is equaivalent to 80rem or 1280px
    <div className="App" class="max-w-7xl justify-center w-full mx-auto">

     <Card/>

    <div class="mx-8 py-6 text-3xl font-medium"> 
        <h class="text-blue-500">Trade Feed </h> <h class="text-blue-900"> · Virtual Co-Invest in the best ideas (405)</h> 
    </div>
    

    <div class="bg-white">
     <Delta/>
   

     <div class="mx-8 py-8 "> 
      <div class="h-20 md:grid sm:flex-wrap grid-cols-3 grid-flow-col gap-2 mx-8">
        <div class="flex-row"><p class="text-blue-900 font-semibold py-2">ATTACHMENTS</p><p>Not specified</p></div>
        <div class="flex-row"><p class="text-blue-900 font-semibold py-2">TIME FRAME</p><p>6 months to 1 year</p></div>
        <div class="flex-row"><p class="text-blue-900 font-semibold py-2">CONVICTION LEVEL </p><button class="tracking-wide text-green-700 font-semibold bg-green-200 justify-items-center rounded-lg mx-auto px-6"> HIGH</button></div>
        <div class="flex-row"><p class="text-blue-900 font-semibold py-2">CATALYST</p><p>Not specified</p></div>
  
      </div>
     </div>

     <div class="max-w-6xl mx-auto w-full sm:h-full md:h-60 bg-indigo-100 rounded-2xl shadow-md my-44">
          <div class="md:flex-wrap p-8 mx-6 text-blue-900 truncate-hidden"> <p class="text-xl  text-blue-800 font-semibold">EXECUTIVE SUMMARY</p> <br/>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus commodo neque rhoncus suscipit dapibus. Mauris dictum mauris at metus lacinia lacinia. 
            Quisque fermentum pulvinar est. Suspendisse leo justo, molestie nec neque ut, consectetur fermentum velit. Praesent pulvinar dui quam, eu tincidunt purus luctus ac. 
            Etiam eget velit tortor. Nunc quam velit, sagittis eu scelerisque vel, commodo egestas massa. Sed tincidunt viverra lectus, in hendrerit velit fringilla vel. 
            Praesent convallis est malesuada ligula dignissim gravida. In pharetra sollicitudin placerat
            </p>
          </div>
     </div>


    <div class="mx-8 px-8 py-8 content-center"> 
      <div class="h-64 grid sm:grid-flow-row-3 md:grid-cols-3 md:grid-flow-col sm:grid-flow-row md:gap-4 sm:gap-2 justify-around">
        <div class="flex"><p class="text-blue-900 font-semibold px-2">Idea submitted</p><p class="text-gray-400">10.10.10</p></div>
        <div class="flex-row"><p class="bg-blue-700 text-blue-100 font-semibold py-2 rounded-lg md:w-2/3 sm:w-full text-center sm:text-sm md:text-base">CO-INVEST IN THIS IDEA</p></div>
        <div class="flex py-2">
          <p class="h-6 w-6 pl-2 font-semibold bg-green-400 rounded-full"></p>
          <p class="text-green-300 px-2 font-semibold">Live Trade </p></div>
      </div>
     </div>


     </div>
    </div>


  );
}

export default App;
